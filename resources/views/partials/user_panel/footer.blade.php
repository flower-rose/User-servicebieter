<footer>
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a class="language" data-target="#langmodal" data-toggle="modal" href="#">
                        <span class="flag-icon flag-icon-us">
                        </span> English
                    </a>
                </li>
                <li><a href="dashboard.html#"><i class="fa fa-question"></i> Help</a>
                </li></ul>
            </nav>
            <p class="copyright pull-right">
            </p><ul class="list-inline foo_links text-right">
                <li><a href="#"><i class="fa fa-copyright"></i>&nbsp;ALL RIGHTS RESERVED. COPYRIGHT SmartSystems 2018.</a></li>
            </ul>
        <p></p>
    </div>
</footer>

<div class="modal fade" id="langmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="height: 320px; padding: 7px 45px">
            <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="font-size:21px" aria-hidden="true">�</span></button>
                <h4
                    class="modal-title" id="myModalLabel">Select your Favorite Language</h4>
            </div>
            <div class="modal-body text-center">
                <a href="localization/en" class="flags activeflag">
                    <div class="flag-icon flag-icon-us flag-big"></div><br>English<span><i class="fa fa-fw fa-check-circle"></i></span></a>
                <a href="localization/hi"
                    class="flags">
                    <div class="flag-icon flag-icon-in flag-big"></div><br>Hindi<span><i class="fa fa-fw fa-check-circle"></i></span></a>
                <a href="localization/ch" class="flags">
                    <div class="flag-icon flag-icon-ch flag-big"></div><br>China<span><i class="fa fa-fw fa-check-circle"></i></span></a>
                <a href="localization/af" class="flags">
                    <div class="flag-icon flag-icon-af flag-big"></div><br>Afria<span><i class="fa fa-fw fa-check-circle"></i></span></a>
            </div>
        </div>
    </div>
</div>