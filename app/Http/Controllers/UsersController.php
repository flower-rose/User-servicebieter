<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function insert_keyword($language_id){
        $email = Auth::user()->email;


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function personalstore(Request $request)
    {

        // echo $request->location;exit;
        
        $email = Auth::user()->email;
        
        DB::table('users')
        ->where('email', $email)
        ->update([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'birthday'=>$request->birthday,
            'location'=>$request->location,
            'phone'=>$request->phone,
            ])
            ;
            
        // echo $request;exit;

        return $request->first_name; 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function emailstore(Request $request)
    {

        echo '123';exit;
        
        $email = Auth::user()->email;
        
        DB::table('users')
        ->where('email', $email)
        ->update([
            'email'=>$request->email,
            'password'=>bcrypt($request->passowrd)
            ]);
            
        // echo $request;exit;

        return $request->first_name; 
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $email = Auth::user()->email;

        $user_question = DB::table('users')->where('email',$email)->get();

        // echo $user_question;exit;

        return $user_question;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit(Users $users)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $users)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $users)
    {
        //
    }
}
