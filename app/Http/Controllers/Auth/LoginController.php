<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Auth;
use Illuminate\Support\Facades\DB;
use \stdClass;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('guest')->except('logout');
        $this->client = new Client([
            'base_uri' => env('MIX_API_BASE_URL')
        ]);
    }

    protected function attemptLogin(Request $request){
        $credentials = $this->credentials($request);
        $isLogin = $this->guard()->attempt($credentials, $request->filled('remember'));
        
        //get current_session 2:User Dashboard
        $section_id = 2;
        
        // get the language_id
        $email = Auth::user()->email;
        $lang_id = DB::table('users')->where('email',$email)->select('lang')->get();    
        
        //get the keyword and value from lang_id and session
        $value_keyword = DB::table('language_keywords')     
        ->where('language_keywords.system_section_id','=',2)
        ->join('language_keyword_mappings', 'language_keywords.id', '=', 'language_keyword_mappings.language_keyword_id')
        ->select('language_keyword_mappings.value', 'language_keyword_mappings.language_keyword_id', 'language_keywords.keyword')
        ->where('language_keyword_mappings.language_id','=',$lang_id[0]->lang)
        ->select('language_keyword_mappings.value','language_keywords.keyword')
        ->get();
        
        $v_k_object = new stdClass();
        foreach($value_keyword as $value){
            $key=$value->keyword;
            $v_k_object->$key=$value->value;
        }


        //Store in session : section_id, lang_id, value_keyword
        $request->session()->put('current_session',$section_id);   
        $request->session()->put('current_language', $lang_id);
        $request->session()->put('keyword_value',$v_k_object);

        $retVal = false;
        if($isLogin){
            $response = $this->client->post('/api/v1/login', [
                'form_params' => [
                    'email' => $credentials['email'],
                    'password' => $credentials['password']
                ]
            ]);
            if($response->getStatusCode() === 200){
                $data = json_decode($response->getBody());
                if(isset($data->success) AND isset($data->success->token)){
                    session(['api-token'=>$data->success->token]);
                    $retVal = true;
                }
            }
        }
        return $retVal;
    }
}
