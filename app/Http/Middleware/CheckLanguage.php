<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;
use Auth;
use Illuminate\Support\Facades\DB;
use \stdClass;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->language_keyword)
        {

            $email = Auth::user()->email;

            // $lang_id = DB::table('users')->where('email',$email)->select('lang')->get();    
            
            $lang_id = DB::table('languages')
            ->where('name','=',$request->language_keyword)
            ->select('id')
            ->get();


            DB::table('users')
            ->where('email', $email)
            ->update([
                'lang'=>$lang_id
            ]);

            //get the keyword and value from lang_id and session
            $value_keyword = DB::table('language_keywords')     
            ->where('language_keywords.system_section_id','=',2)
            ->join('language_keyword_mappings', 'language_keywords.id', '=', 'language_keyword_mappings.language_keyword_id')
            ->select('language_keyword_mappings.value', 'language_keyword_mappings.language_keyword_id', 'language_keywords.keyword')
            ->where('language_keyword_mappings.language_id','=',$lang_id[0]->id)
            ->select('language_keyword_mappings.value','language_keywords.keyword')
            ->get();

            $v_k_object = new stdClass();
            foreach($value_keyword as $value){
                $key=$value->keyword;
                $v_k_object->$key=$value->value;
            }
    
            //Store in session : section_id, lang_id, value_keyword
            $request->session()->put('current_language', $lang_id[0]->id);
            $request->session()->put('keyword_value',$v_k_object);

            return redirect(URL::previous());
        }
        return $next($request);
    }
}
