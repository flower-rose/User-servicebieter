<?php

use Illuminate\Database\Seeder;
use App\SystemSection;
use App\LanguageKeyword;

class LanguageKeywordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $systemSections = SystemSection::all();

        foreach($systemSections as $systemSection) {
            LanguageKeyword::create([
                'keyword' => 'activity',
                'system_section_id' => $systemSection->id
            ]);
        }
    }
}
