<?php

use Illuminate\Database\Seeder;
use App\SystemSection;

class SystemSectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemSection::insert([
            ['name' => 'Main Page'],
            ['name' => 'User Dashboard'],
            ['name' => 'Company Dashboard'],
            ['name' => 'Admin'],
            ['name' => 'Email Notifications'],
        ]);
    }
}
